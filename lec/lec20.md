
# Lecture 20: Overview, review and looking forward


## Exam in January

-   Friday 14 Jan 2022, 9:00

-   ~~3 hours long~~ 2 hours long

-   6 questions totalling 60 marks.

-   Each question has multiple parts

-   Submission is via Gradescope either through short numerical answers, multiple choice or free response text boxes (no uploads)

### Past papers

Some exams from recent years are on minerva

-   These were set by someone else
-   They were closed book
-   They are too long
-   Ignore questions on Newton interpolation.

### Examinable material

In this module **everything** may be assessed:

-   All material in lectures;

-   All algorithmic details contained within the sample codes (that accompany the lectures, worksheets or coursework);

-   All material in formative worksheets and summative courseworks.

### What's expected and what to expect

I will expect you to be **able to apply** and **understand** all of the algorithms discussed:

-   I will expect you to understand when to use an algorithm.

-   I will expect you to understand how to analyse an algorithm either based on the algorithm itself or numerical results from the algorithm.

-   I will expect you to be able to understand variations and generalisations of algorithms, and to implement them.

-   I will expect you to understand and apply simple derivatives.

## Review

-   Floating point numbers
    -   Representations and rounding
    -   $eps$, machine precision
-   Matrices and vectors
    -   Multiplication
    -   Inner product
    -   Euclidean norms

### Review (cont.)

-   Systems of linear equations
    -   Translating between matrices and systems of linear equations
    -   Deriving simple linear systems of equations
    -   Solving triangular systems of equations
    -   Gaussian Elimination
    -   LU factorisation
    -   Jacobi iteration
    -   Gauss Seidel iteration
    -   Initial guesses, convergence and stopping criteria

### Review (cont.)

-   Dynamic problems
    -   Derivatives, gradients/slopes and rates of change
    -   Reading and drawing simple graphs
    -   Deriving models for simple dynamic problems
    -   Initial conditions
    -   Euler's method
    -   Midpoint scheme

### Review (cont.)

-   Nonlinear equations
    -   Roots of an equation
    -   Bisection algorithm
    -   Newton's method
    -   Quasi Newton methods
    -   Hybrid methods

### Review (cont.)

-   Data fitting
    -   How to form a system of linear equations to find a simple curve of best fit
    -   How to find a best fit solution
    -   When to choose which curve (from a simple choice)
    -   Problems with our approach

### Python

For this module, you are expected to be able to read, understand and construct simple scripts and functions, to implement simple algorithms, or to modify algorithms for your needs, and to be able to present and interpret the outputs from these. To this end, you should be familiar with elementary python notation and syntax (in particular, vectors, matrices, indices, loops and branches).

## Tips for online exams

-   Open book exam tips from library: [Online open book exams](https://library.leeds.ac.uk/info/1401/academic_skills/191/online_open_exams)

-   Questions will test both **application** and **understanding**.

-   You will need to be able to know information or to recall it quickly.

-   Save your work regularly

-   Computer lab open

## What next....?

-   You will apply ideas from this course in many areas of computer science including

    -   graphics;
    -   artificial intelligence/machine learning.

-   There are plenty of opportunities for Final Year Projects on topics in numerical computation. If you are interested talk to me (or your tutorial leader).

-   There are also funded places for further study in numerical computation....

### 

**Thank you for your attention!**

Good luck with your exam and future studies

I hope you have a good Christmas break!
